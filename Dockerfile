FROM debian:11

RUN apt-get update                    \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get install -y            \
            apt-utils                 \
            autoconf                  \
            git                       \
            python3-pip               \
            scons                     \
            sudo                      \
            unzip                     \
            valgrind                  \
            yasm                      \
            zip                       \
    && apt-get clean                  \
    && rm -rf /var/lib/apt/lists/*

# Allow password-less sudo
RUN echo 'ALL ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers

# TODO: Add Unity and CMock

# Install pip dependencies
COPY ./requirements.txt ./
RUN pip3 install -r requirements.txt && rm *.txt
